import { css } from "solid-styled-components";
import ThemeSwitch from "../components/ThemeSwitch";
import store from "../store";

const style = css`
.switch {
	display: flex;
	align-items: center;
	padding: 1em;
	span {
		margin-right: auto;
	}
}
a {
	display: flex;
	align-items: center;
	padding: 1em;
	color: #222;
	svg {
		width: 2em;
		height: 2em;
		margin-right: .5em;
	}
}
&.dark {
	a {
		color: #fff;
	}
}
`

export default function() {	
	const [dark, toggleDark] = store
	return (
		<div class={style} classList={{dark: dark()}}>
			<div class="switch" onClick={toggleDark} data-ripplet>
				<span>Dark Theme</span>
				<ThemeSwitch />
			</div>
			<a target="_blank" href="https://codeberg.org/bramaudi/qrman" data-ripplet>
				<svg xmlns="http://www.w3.org/2000/svg" width="1.2em" height="1.2em" viewBox="0 0 24 24"><path fill="currentColor" d="M11.955.49A12 12 0 0 0 0 12.49a12 12 0 0 0 1.832 6.373L11.838 5.928a.187.14 0 0 1 .324 0l10.006 12.935A12 12 0 0 0 24 12.49a12 12 0 0 0-12-12a12 12 0 0 0-.045 0m.375 6.467l4.416 16.553a12 12 0 0 0 5.137-4.213z"></path></svg>
				Source Code
			</a>
		</div>
	)
}