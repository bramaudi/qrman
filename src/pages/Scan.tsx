import { createSignal, onMount } from 'solid-js'
import { createStore } from 'solid-js/store'
import { Html5Qrcode } from "html5-qrcode"
import { css } from 'solid-styled-components'
import store from '../store.jsx'


const style = css`
	input[type=file] {
		display: none;
	}
	.buttons {
		display: flex;
		justify-content: center;
		padding: 1em;
	}
	[class*=btn-] {
		cursor: pointer;
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
		width: 8em;
		height: 8em;
		padding: .5em;
		margin: .5em;
		border-radius: 100%;
		background: cornflowerblue;
		color: white;
		border: 1px solid transparent;

		svg {
			width: 3em;
			height: 3em;
		}
	}
	.webcam {
		align-items: center;
		justify-content: center;
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background: #f0f0f0;
	}
	.preview {
		padding: 1em;
		padding-top: 0;

		img {
			max-height: 10em;
			margin: auto;
		}
	}
	.back {
		position: fixed;
		left: 50%;
		transform: translateX(-50%);
		z-index: 3;
		bottom: 2rem;
		padding: .3em 1em;
		text-decoration: none;
		border-radius: 2rem;
		cursor: pointer;
		border: none;
		color: white;
		background: cornflowerblue;
	}
	.result {
		padding: 1em;
		padding-top: 0;

		.result-box {
			word-wrap: break-word;
			width: 100%;
			padding: .5em;
			border: 1px solid #acabab;
			border-radius: .5em;
			-webkit-user-select: text;
			-moz-user-select: text;
			-ms-user-select: text;
			user-select: text;
		}
	}

	&.dark {
		.webcam {
			background-color: #222;
		}
		.result {
			textarea {
				border-color: #555;
				background-color: #333;
				color: #fff;
			}
		}
	}
`

export default function () {
	let scanner!: Html5Qrcode
	let refImage!: HTMLImageElement
	let refResult!: HTMLDivElement
	const [dark] = store
	const [state, setState] = createStore({
		video: false,
		image: false,
	})
	const [rawResult, setResult] = createSignal('')

	function stopCamera() {
		scanner.stop()
		setState('video', false)
	}
	async function openCamera() {
		try {
			const hasCamera = await Html5Qrcode.getCameras()
			if (hasCamera) {
				setState('image', false)
				setState('video', true)
				await scanner.start(
					{ facingMode: "environment" },
					{ fps: 10, qrbox: 150 },
					(decodedText: string) => {
						setResult(decodedText)
						stopCamera()
					},
					undefined
				)
			}
		} catch (error) {
			alert(error)
		}
	}
	function handlePreviewImage(files: FileList | null) {
		if (files?.length) {
			setResult('Processing image ...')

			const dataUri = URL.createObjectURL(files[0])
			refImage.src = dataUri
			refImage.onload = () => URL.revokeObjectURL(refImage.src)
			setState('image', true)

			scanner.scanFile(files[0]).then((data: string) => {
				setResult(data)
			}).catch((error: { message: string }) => {
				setResult(error.message)
			});
		}
	}
	function result() {
		const link = /(https?:\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?)/g
		const localhost = /(https?:\/\/localhost([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?)/g
		return rawResult()
			.replace(link, '<a target="_blank" href="$1">$1</a>')
			.replace(localhost, '<a target="_blank" href="$1">$1</a>')
			.replaceAll('\n', '<br/>')
	}

	onMount(() => {
		scanner = new Html5Qrcode('reader')
	})

	return (
		<div class={style} classList={{ dark: dark() }}>
			<input
				id="inputImage"
				type="file"
				accept="image/*"
				onChange={e => handlePreviewImage(e.currentTarget.files)}
			/>
			<div class="buttons">
				<button onClick={openCamera} class="btn-camera" data-ripplet>
					<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M19 6.5h-1.28l-.32-1a3 3 0 0 0-2.84-2H9.44A3 3 0 0 0 6.6 5.55l-.32 1H5a3 3 0 0 0-3 3v8a3 3 0 0 0 3 3h14a3 3 0 0 0 3-3v-8a3 3 0 0 0-3-3.05Zm1 11a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-8a1 1 0 0 1 1-1h2a1 1 0 0 0 1-.68l.54-1.64a1 1 0 0 1 .95-.68h5.12a1 1 0 0 1 .95.68l.54 1.64a1 1 0 0 0 .9.68h2a1 1 0 0 1 1 1Zm-8-9a4 4 0 1 0 4 4a4 4 0 0 0-4-4Zm0 6a2 2 0 1 1 2-2a2 2 0 0 1-2 2Z" /></svg>
					<div>Camera</div>
				</button>
				<label for="inputImage" class="btn-image" data-ripplet>
					<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24"><path fill="currentColor" d="M19 10a1 1 0 0 0-1 1v3.38l-1.48-1.48a2.79 2.79 0 0 0-3.93 0l-.7.71l-2.48-2.49a2.79 2.79 0 0 0-3.93 0L4 12.61V7a1 1 0 0 1 1-1h8a1 1 0 0 0 0-2H5a3 3 0 0 0-3 3v12.22A2.79 2.79 0 0 0 4.78 22h12.44a2.88 2.88 0 0 0 .8-.12a2.74 2.74 0 0 0 2-2.65V11A1 1 0 0 0 19 10ZM5 20a1 1 0 0 1-1-1v-3.57l2.89-2.89a.78.78 0 0 1 1.1 0L15.46 20Zm13-1a1 1 0 0 1-.18.54L13.3 15l.71-.7a.77.77 0 0 1 1.1 0L18 17.21Zm3-15h-1V3a1 1 0 0 0-2 0v1h-1a1 1 0 0 0 0 2h1v1a1 1 0 0 0 2 0V6h1a1 1 0 0 0 0-2Z" /></svg>
					<div>Gallery</div>
				</label>
			</div>

			<div
				class="webcam"
				style={{ display: state.video ? 'flex' : 'none' }}
				data-keen-slider-clickable
			>
				<div id="reader"></div>
			</div>
			<button onClick={stopCamera} class="back" style={{ display: state.video ? 'block' : 'none' }}>&laquo; Go back</button>

			<div class="preview" style={{ display: state.image ? 'block' : 'none' }}>
				<img
					ref={refImage}
					alt="Broken image"
				/>
			</div>
			<div
				class="result"
				style={{ display: result().length ? 'block' : 'none' }}
			>
				<div
					ref={refResult}
					class="result-box"
					data-keen-slider-clickable
					innerHTML={result()}
				/>
			</div>
		</div>
	);
}