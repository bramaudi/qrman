import { createEffect, createSignal, onMount } from 'solid-js'
import { css } from 'solid-styled-components'
import store from '../store'
import QRCode from 'qrcode'

const style = css`
	padding: 1em;
	overflow: hidden;

	> textarea {
		max-width: 100%;
		width: 100%;
		height: 10em;
		padding: .5em;
		margin-bottom: 1em;
		border-radius: .3em;
		border: 1px solid #ddd;
	}

	.scale {
		display: flex;
		align-items: center;
		justify-content: center;
		margin-bottom: 1em;

		.radio {
			margin: 0 .5em;
			display: flex;
			align-items: center;
			justify-content: center;
			
			input[type=radio] {
				margin: 0 .3em 0;
			}
		}
	}
	img {
		margin: 1em auto;
	}

	.btn {
		position: absolute;
		z-index: 0;
		left: 0;
		right: 0;
		cursor: pointer;
		width: 10em;
		margin: auto;
		padding: .5em 1em;
		text-align: center;
		border: none;
		border-radius: .3em;
		background: #222;
		color: #ddd;
	}

	&.dark {
		> textarea {
			border-color: #555;
			background-color: #333;
			color: #fff;
		}
		.btn {
			background-color: #555;
		}
	}
`

export default function () {
	const [dark] = store
	const [text, setText] = createSignal('')
	const [result, setResult] = createSignal('')
	const [scale, setScale] = createSignal(0)

	async function generateQR(value: string) {
		setText(value)
		try {
			if (value.length) {
				const dataUri = await QRCode.toDataURL(value, {
					errorCorrectionLevel: 'H',
					type: 'image/jpeg',
					scale: scale(),
					margin: 1
				})
				setResult(dataUri)
			}
		} catch (err) {
			console.error(err)
		}
	}
	function loadParamQuery() {
		const url = new URL(window.location.href)
		const param = url.searchParams.get('text')
		if (param?.length) {
			setText(decodeURIComponent(param))
		}
	}
	function syncParamQuery() {
		const url = new URL(window.location.href)
		url.searchParams.set('text', encodeURIComponent(text()))
		window.history.replaceState('', '', url.href);
	}
	function slugify(text: string) {
		return text.toString().toLowerCase()
			.replace(/\s+/g, '-')           // Replace spaces with -
			.replace(/[^\w\-]+/g, '')       // Remove all non-word chars
			.replace(/\-\-+/g, '-')         // Replace multiple - with single -
			.replace(/^-+/, '')             // Trim - from start of text
			.replace(/-+$/, '');            // Trim - from end of text
	}

	onMount(() => {
		loadParamQuery()
	})
	createEffect(() => {
		generateQR(text())
		syncParamQuery()
	})

	return (
		<div class={style} classList={{ dark: dark() }}>
			<textarea
				id="apk__input"
				placeholder="Input text"
				onInput={(e) => setText(e.currentTarget.value)}
				data-keen-slider-clickable
				value={text()}
			/>
			<div class="scale">
				<div class="radio">
					<input onClick={() => setScale(0)} type="radio" name="scale" id="scaleSmall" checked />
					<label for="scaleSmall">Small</label>
				</div>
				<div class="radio">
					<input onClick={() => setScale(8)} type="radio" name="scale" id="scaleMedium" />
					<label for="scaleMedium">Medium</label>
				</div>
				<div class="radio">
					<input onClick={() => setScale(15)} type="radio" name="scale" id="scaleLarge" />
					<label for="scaleLarge">Large</label>
				</div>
			</div>
			{/* Result */}
			<img
				id="qr-result"
				src={result()}
				style={{ display: text().length ? 'block' : 'none' }}
			/>
			<a
				style={{ display: text().length ? 'block' : 'none' }}
				class="btn"
				id="apk__download"
				href={result()}
				download={`qrman_${slugify(text()).substring(0, 20)}.jpg`}
				data-ripplet
			>Download</a>
			<div style={{margin: '4em 0 0 0'}}></div>
		</div>
	)
}
