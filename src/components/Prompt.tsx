import { useRegisterSW } from 'virtual:pwa-register/solid'
import { createEffect, Show } from 'solid-js'
import { css } from 'solid-styled-components'
import store from '../store'

const style = css`
	.notify {
		position: fixed;
		z-index: 1;
		max-width: 480px;
		bottom: 0;
		margin: 1em;
		padding: 1em;
		border-radius: .5em;
		background-color: #91ffbf;
		border: 1px solid #41cc3c;
		box-shadow: 0 1px 3px 0 rgb(0 0 0 / 0.1), 0 1px 2px -1px rgb(0 0 0 / 0.1);;

		&.blue {
			background-color: #91d3ff;
			border-color: #3cb1cc;
		}

		button {
			cursor: pointer;
			display: block;
			padding: .3em .5em;
			margin-top: .5em;
			border: none;
			border-radius: .3em;
			background: #222;
			color: #ddd;
		}
	}

	&.dark {
		.notify {
			background-color: #214629;
			border-color: green;
			box-shadow: 0 5px 10px 1px rgba(50, 50, 50, 0.2);

			&.blue {
				background-color: #213a46;
				border-color: #005180;
			}
		}
	}
`

export default function() {
	const [dark] = store
	const { offlineReady, needRefresh, updateServiceWorker } = useRegisterSW()
	const [offline, setOffline] = offlineReady
	const [refresh] = needRefresh

	createEffect(() => {
		if (offline()) {
			setTimeout(() => {
				setOffline(false)
			}, 3000)
		}
	})

	return (
		<div class={style} classList={{dark: dark()}}>
		<Show when={offline()}>
			<div class="notify">
				App ready to work offline.
			</div>
		</Show>
		<Show when={refresh()}>
			<div class="notify blue">
				New content available!
				<button onClick={() => updateServiceWorker(true)} data-ripplet>Refresh</button>
			</div>
		</Show>
		</div>
	)
}