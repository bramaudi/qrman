import type { Component, JSX } from 'solid-js'

type Props = { navPos: number, children: JSX.Element }
const Layout: Component<Props> = (props) => {
	return (
		<div class="layout">
			<nav>
				<button classList={{active: props.navPos == 0}} data-ripplet>Scan</button>
				<button classList={{active: props.navPos == 1}} data-ripplet>Create</button>
				<button classList={{active: props.navPos == 2}} data-ripplet>Menu</button>
			</nav>
			<main>
				{props.children}
			</main>
		</div>
	)
}

export default Layout