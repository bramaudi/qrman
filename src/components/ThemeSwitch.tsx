import { css } from "solid-styled-components"
import store from "../store"

const style = css`
cursor: pointer;
display: flex;
border-radius: 9999px;
height: 25px;
width: 50px;

/* OFF status */
background-color: rgba(0, 0, 0, 0.1);
border: 1px solid rgba(0, 0, 0, 0.3);

&.on {
	/* ON status */
	background-color: #357edd;
	border: 1px solid #357edd;
	/* Push the circle to the right */
	justify-content: flex-end;
}

	.label__input {
		display: none;
	}

	.label__circle {
		border-radius: 9999px;
		width: 25px;
		background-color: #fff;
		border: 1px solid rgba(0, 0, 0, 0.3);
	}
`
export default function() {
	const [dark, toggleDark] = store
	return (
		<label class={style} for="themeSwitch" classList={{on: dark()}}>
			<input id="themeSwitch" type="checkbox" class="label__input" onClick={toggleDark} />

			{/* Circle */}
			<div class="label__circle"></div>
		</label>
	)
}