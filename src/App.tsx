import { createEffect, createSignal, onMount } from "solid-js";
import { createSlider } from "solid-slider";
import Create from "./pages/Create";
import Scan from "./pages/Scan";
import Menu from "./pages/Menu";
import store from "./store";	
import Prompt from "./components/Prompt";

export default function () {
	const [dark] = store
	const [tab, setTab] = createSignal(0)
	const [slider, { moveTo }] = createSlider()

	function detectParamQuery() {
		const url = new URL(window.location.href)
		const param = url.searchParams.get('text')
		if (param?.length) {
			setTab(1)
			setTimeout(() => {
				moveTo(1)
			})
		}
	}

	onMount(() => {
		detectParamQuery()
		if (dark()) {
			document.body.classList.add('dark')
		}
	})
	createEffect(() => {
		moveTo(tab())
		window.scrollTo({ top: 0, behavior: 'smooth' })
	})

	slider;
	return (
		<>
			<nav classList={{dark: dark()}}>
				<button onClick={() => setTab(0)} classList={{active: tab() === 0}} data-ripplet>Scan</button>
				<button onClick={() => setTab(1)} classList={{active: tab() === 1}} data-ripplet>Create</button>
				<button onClick={() => setTab(2)} classList={{active: tab() === 2}} data-ripplet>Menu</button>
			</nav>
			<Prompt />
			<main use:slider>
				<Scan />
				<Create />
				<Menu />
			</main>
		</>
	)
}