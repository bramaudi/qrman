/* @refresh reload */
import { render } from 'solid-js/web';

import './index.css';
import 'solid-slider/slider.css';
import 'ripplet.js/es/ripplet-declarative'
import App from './App';

render(() => <App />, document.getElementById('root') as HTMLElement);
