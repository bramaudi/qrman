import { Accessor, createSignal } from 'solid-js'

const [dark, setDark] = createSignal(window.localStorage.getItem('dark') === 'true')

function switchTheme() {
	const theme = !dark()
	setDark(theme)
	window.localStorage.setItem('dark', theme.toString())
	if (theme) document.body.classList.add('dark')
	else document.body.classList.remove('dark')
}

const store: [Accessor<boolean>, () => void] = [dark, switchTheme]
export default store