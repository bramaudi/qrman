# qrman

Simple web based QR Code tools.

## Features

- Scan QR Code (from Camera / Gallery)
- Create QR Code
- Easy to share/generate via link (https://qrman.vercel.app/?text=hello)

## Development

This project using [Bun](https://bun.sh/) as package manager.

```bash
# install dependencies
bun i

# start development server
bun dev

# build production
bun run build

# make certificate for local SSL development
bun run cert
```

> If you can't start webcam in development this mean WebRTC are unusable without SSL, you need to make local certificate and uncomment `server.https` in `vite.config.ts` to run localhost in HTTPS.

---

Credit:
- Logo & icons are from [Unicons](https://icon-sets.iconify.design/uil/)
- UI Library: [Solid-JS](https://solidjs.com)
- QR Scanner: https://github.com/mebjas/html5-qrcode
- QR Generator: https://github.com/soldair/node-qrcode