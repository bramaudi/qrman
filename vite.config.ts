import { readFileSync } from 'fs'
import { defineConfig } from 'vite';
import PluginSolid from 'vite-plugin-solid';
import { VitePWA as PluginPWA } from 'vite-plugin-pwa'

const pwaConfig = {
  workbox: { inlineWorkboxRuntime: true },
  includeAssets: ['favicon.svg', 'favicon.ico', 'robots.txt', 'apple-touch-icon.png'],  
  manifest: {
    name: 'QRMan',
    short_name: 'QRMan',
    description: 'Simple, Focus, and Easy QR Tool',
    theme_color: '#6495ed',
    icons: [
      {
        src: 'pwa-192x192.png',
        sizes: '192x192',
        type: 'image/png',
      },
      {
        src: 'pwa-512x512.png',
        sizes: '512x512',
        type: 'image/png',
      },
      {
        src: 'pwa-512x512.png',
        sizes: '512x512',
        type: 'image/png',
        purpose: 'any maskable',
      }
    ]
  }
}
export default defineConfig({
  plugins: [PluginSolid(), PluginPWA(pwaConfig)],
  build: {
    target: 'esnext',
    polyfillDynamicImport: false,
  },
  server: {
    host: '0.0.0.0',
    port: 3000,
    // https: {
    //   key: readFileSync('./.cert/key.pem'),
    //   cert: readFileSync('./.cert/cert.pem'),
    // }
  }
});
